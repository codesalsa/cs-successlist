<?php

//add_action( $tag, $function_to_add, $priority, $accepted_args );
add_action('admin_menu', 'cs_successlist_menu'); 

function cs_successlist_menu(){
    //add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
    add_menu_page(' Success List ', ' Success List ', 'administrator', 'cs_successlist_options', 'cs_successlist_main_page');

    //add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function );
    add_submenu_page('cs_successlist_options', 'Success List Settings', 'Settings', 'administrator', 'cs_successlist_settings', 'cs_successlist_settings_page'); 

    //add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function );
    add_submenu_page('cs_successlist_options', 'Success List - Add Items', 'Add List Item', 'administrator', 'cs_successlist_add', 'cs_successlist_add_page'); 
    
    //add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function );
    add_submenu_page('cs_successlist_options', 'Success List - Import CSV Items', 'Import CSV Items', 'administrator', 'cs_successlist_import', 'cs_successlist_import_page'); 


}

