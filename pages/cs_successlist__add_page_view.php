<?php
function cs_successlist_add_page() {

    global $mfwp_options;
    ob_start();
    date_default_timezone_set('UTC');

    if (isset($_POST['insert'])) {
    
        $name = $_POST["name"];
        $visa_type = $_POST["visa_type"];
        $country = $_POST["country"];
        $category = $_POST["category"];
        $results = $_POST["results"];
        $month = $_POST["month"];
        $year = $_POST["year"];

    //insert
        global $wpdb;
        $table_name = $wpdb->prefix . "cs_successlist";
        $wpdb->insert(
                $table_name, //table
                array(
                    'name' => $name,
                    'visa_type' => $visa_type,
                    'country' => $country,
                    'category' => $category,
                    'results' => $results,
                    'month' => $month,
                    'year' => $year
                ), //data
                array('%s', '%s', '%s', '%s', '%s', '%s', '%d') //data format			
        );
        $message = "<div class='notice notice-success is-dismissible'><p>Success: List Item Added successfully.</p></div>";

    }
    ?>
    
    <div class="wrap">
        <div class="cs-section-heading">
            <h1>Add New Success List Item <a class="btn" href="admin.php?page=cs_successlist_import">Import Success List CSV</a></h1>
        </div>
        <?php if (isset($message)): echo $message; endif; ?>
        <div class="cs-section-content">
            <div class="cs-section-main">
                <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">

                    <div class="form-container">
                        <div class="form-control">
                            <label for="name">Name</label>
                            <input id="name" type="text" name="name" placeholder="Enter Full Name" class="ss-field-width" required />
                        </div>
                        <div class="form-control">
                            <label for="visa_type">Visa Type</label>
                            <input id="visa_type" type="text" name="visa_type" placeholder="Enter Visa Type" class="ss-field-width" required />
                        </div>
                        <div class="form-control">
                            <label for="country">Country</label>
                            <input id="country" type="text" name="country" placeholder="Enter Country" class="ss-field-width" required />
                        </div>
                        <div class="form-control">
                            <label for="category">Category</label>
                            <input id="category" type="text" name="category" placeholder="Enter Category" class="ss-field-width" required />
                        </div>
                        <div class="form-control">
                            <label for="results">Results</label>
                            <input id="results" type="text" name="results" placeholder="Enter Results" class="ss-field-width" required />
                        </div>
                        <div class="form-control select-fld-block">
                            <label for="month">Month</label>
                            <?php $currentMonth = date("F"); ?>
                            <select name="month" id="month"  class="ss-field-width" required>
                                <option value="January" <?php if($currentMonth == 'January'){echo "selected"; } ?>>January</option>
                                <option value="February" <?php if($currentMonth == 'February'){echo "selected"; } ?>>February</option>
                                <option value="March" <?php if($currentMonth == 'March'){echo "selected"; } ?>>March</option>
                                <option value="April" <?php if($currentMonth == 'April'){echo "selected"; } ?>>April</option>
                                <option value="May" <?php if($currentMonth == 'May'){echo "selected"; } ?>>May</option>
                                <option value="June" <?php if($currentMonth == 'June'){echo "selected"; } ?>>June</option>
                                <option value="July" <?php if($currentMonth == 'July'){echo "selected"; } ?>>July</option>
                                <option value="August" <?php if($currentMonth == 'August'){echo "selected"; } ?>>August</option>
                                <option value="September" <?php if($currentMonth == 'September'){echo "selected"; } ?>>September</option>
                                <option value="October" <?php if($currentMonth == 'October'){echo "selected"; } ?>>October</option>
                                <option value="November" <?php if($currentMonth == 'November'){echo "selected"; } ?>>November</option>
                                <option value="December" <?php if($currentMonth == 'December'){echo "selected"; } ?>>December</option>
                            </select>
                        </div>
                        <div class="form-control select-fld-block">
                            <label for="year">Year</label>
                            <?php $currentYear = date("Y"); ?>
                            <select name="year" id="year"  class="ss-field-width" required>
                                <option value="2016" <?php if($currentYear == '2016'){echo "selected"; } ?>>2016</option>
                                <option value="2017" <?php if($currentYear == '2017'){echo "selected"; } ?>>2017</option>
                                <option value="2018" <?php if($currentYear == '2018'){echo "selected"; } ?>>2018</option>
                                <option value="2019" <?php if($currentYear == '2019'){echo "selected"; } ?>>2019</option>
                                <option value="2020" <?php if($currentYear == '2020'){echo "selected"; } ?>>2020</option>
                                <option value="2021" <?php if($currentYear == '2021'){echo "selected"; } ?>>2021</option>
                                <option value="2022" <?php if($currentYear == '2022'){echo "selected"; } ?>>2022</option>
                            </select>
                        </div>
                        <div class="form-control">
                            <input type='submit' name="insert" value='Save Record' class='button' />
                        </div>
                    </div>
                </form>
            </div>
        
            <aside class="cs-section-sidebar">

            </aside>
        
        </div>

    </div>
    <?php
}
echo ob_get_clean();