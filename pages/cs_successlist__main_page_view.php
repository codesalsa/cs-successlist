<?php 
function cs_successlist_main_page() {
 
	global $mfwp_options;
 
	ob_start(); ?>
		<div class="wrap">
			<div class="cs-section-heading">
				<h1>Add New Success List Item <a class="btn" href="admin.php?page=cs_successlist_import">Import Success List CSV</a></h1>
			</div>
			<div class="cs-section-content">
            	<div class="cs-section-main width-100">
					<?php
						global $wpdb;
						$table_name = $wpdb->prefix . "cs_successlist";
						$rows = $wpdb->get_results("SELECT * from $table_name");
					?>
					<table class='wp-list-table widefat fixed striped posts'>
						<thead>
							<tr>
								<th class="manage-column ss-list-width">#</th>
								<th class="manage-column ss-list-width">Name</th>
								<th class="manage-column ss-list-width">Visa Type</th>
								<th class="manage-column ss-list-width">Country</th>
								<th class="manage-column ss-list-width">Category</th>
								<th class="manage-column ss-list-width">Results</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach ($rows as $row) { ?>
							<tr>
								<td class="manage-column ss-list-width"><?php echo $row->id; ?></td>
								<td class="manage-column ss-list-width"><?php echo $row->name; ?></td>
								<td class="manage-column ss-list-width"><?php echo $row->visa_type; ?></td>
								<td class="manage-column ss-list-width"><?php echo $row->country; ?></td>
								<td class="manage-column ss-list-width"><?php echo $row->category; ?></td>
								<td class="manage-column ss-list-width"><?php echo $row->results; ?></td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>

			</div>
			
		</div>
	<?php
	echo ob_get_clean();
}