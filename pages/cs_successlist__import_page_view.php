<?php
function cs_successlist_import_page() {

    global $mfwp_options;
    ob_start();

    if (isset($_POST['import'])) {
    
        $filename=$_FILES["import_success_csv"]["name"];
        $ext=substr($filename,strrpos($filename,"."),(strlen($filename)-strrpos($filename,".")));
        //$ext = pathinfo($filename, PATHINFO_EXTENSION);


        //we check,file must be have csv extention
        if($ext==".csv")
        {
            $file1 = $_FILES['import_success_csv']['tmp_name'];
            $file = fopen($file1, "r");
            if($file){
                while (($successData = fgetcsv($file, 1000, ",")) !== FALSE)
                {
                    global $wpdb;
                    $table_name = $wpdb->prefix . "cs_successlist";
                    //$month_year = $successData[7].'-'.$successData[6];
                    $wpdb->insert(
                            $table_name, //table
                            array(
                                'name' => $successData[1],
                                'visa_type' => $successData[2],
                                'country' => $successData[3],
                                'category' => $successData[4],
                                'results' => $successData[5],
                                'month' => $successData[7],
                                'year' => $successData[6]
                            ), //data
                            array('%s', '%s', '%s', '%s', '%s', '%s', '%d') //data format			
                    );
                }
            }
            fclose($file);
            ?>
                <div class="notice notice-success is-dismissible">
                    <p>Success: List Item Imported successfully.</p>
                </div>
            <?php
            
        }
        else {

        ?>

        <div class="notice notice-error is-dismissible">
            <p>Error: Please Upload only CSV File</p>
        </div>

        <?php
           
        }

    }
    ?>
    
    <div class="wrap">
        <div class="cs-section-heading">
            <h1>Import CSV Success List Items <a class="btn btn-success" href="admin.php?page=cs_successlist_add">Add Success List Item</a></h1>            
        </div>
        <?php if (isset($message)): ?><div class="updated"><p><?php echo $message; ?></p></div><?php endif; ?>
        <div class="cs-section-content">
            <div class="cs-section-main">
                <form enctype="multipart/form-data" method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
                    <div class="form-container">
                        <div class="form-control">
                            <label>CSV File <small>( Upload only .csv file format )</small></label>
                            <input type="file" name="import_success_csv" id="file" class="form-control-file" />
                        </div>
                        <div class="form-control">
                            <input type='submit' name="import" value='Import Record' class='button' />
                        </div>
                    </div>
                    <?php wp_nonce_field( plugin_basename( __FILE__ ), 'import_success_csv-nonce' ); ?>
                </form>
            </div>
        </div>

    </div>
    <?php
}
echo ob_get_clean();