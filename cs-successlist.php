<?php
/**
 * Plugin Name: CS Success List
 * Plugin URI: https://apexvisas.com
 * Description: A custom plugin for importing and showing Apex Visa's Success List
 * Version: 0.1
 * Author: Codesalsa
 * Author URI: https://codesalsa.net
 * License: GPL2
 */ 


include "functions/plugin_menupage.php"; 

include "pages/cs_successlist__main_page_view.php";
include "pages/cs_successlist__settings_page_view.php"; 
include "pages/cs_successlist__add_page_view.php"; 
include "pages/cs_successlist__import_page_view.php"; 


if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}


/**
 * Enqueue custom Admin scripts and styles.
*/

function cs_successlist_scripts(){

	wp_enqueue_style( 'cs-successlist-custom-style', plugins_url('/assets/css/cs_successlist_admin.css', __FILE__), false);

}

add_action('admin_enqueue_scripts', 'cs_successlist_scripts');

/**
 * Enqueue custom Frontend scripts and styles.
*/

function cs_successlist_frontend_scripts(){

	wp_enqueue_style( 'cs-successlist-custom-frontend-style', plugins_url('/assets/css/cs_successlist_frontend.css', __FILE__), false);

}

add_action('wp_enqueue_scripts', 'cs_successlist_frontend_scripts');


// function to create the DB / Options / Defaults					
function cs_successlist_plugin_options_install() {
    global $wpdb;
    $table_name = $wpdb->prefix . 'cs_successlist';
    $charset_collate = $wpdb->get_charset_collate();

 // create the ECPT metabox database table
 if($wpdb->get_var("show tables like '$table_name'") != $table_name) 
 {
     $sql = "CREATE TABLE $table_name (
         id mediumint(9) NOT NULL AUTO_INCREMENT,
         name tinytext NOT NULL,
         visa_type varchar(100) NOT NULL,
         country varchar(100) NOT NULL,
         category varchar(100) NOT NULL,
         results varchar(100) NOT NULL,
         year int(10) NOT NULL,
         month varchar(30) NOT NULL,
         UNIQUE KEY id (id)
     ) $charset_collate;";

     require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
     dbDelta($sql);
 }

}
// run the install scripts upon plugin activation
register_activation_hook(__FILE__,'cs_successlist_plugin_options_install');


function cs_success_list_shortcode($atts) {
    global $mfwp_options;
 
	ob_start(); 

	$value = shortcode_atts( array(
        'title' => 'All Success List'
    ), $atts );
	
	
	?>
		<div class="wrap">
			<div class="cs-section-heading">
				<h1 id="base-title"><?php echo $value['title']; ?></h1>
				<?php
					global $wpdb;
					$count = 1;
					$table_name = $wpdb->prefix . "cs_successlist";
					$rows = $wpdb->get_results("SELECT * from $table_name  ORDER BY year DESC, FIELD( month, 'December', 'November', 'October', 'September', 'August', 'July', 'June', 'May', 'April', 'March', 'February', 'January' )");
					$month_rows = $wpdb->get_results("SELECT DISTINCT month, year from $table_name ORDER BY year DESC, FIELD( month, 'December', 'November', 'October', 'September', 'August', 'July', 'June', 'May', 'April', 'March', 'February', 'January' )");
				?>
				<div class="filter-fld-cont">
					<select id="cs_successListFilter">
						<option  class="item" value="">All</option>
						<?php foreach ($month_rows as $month_row) { ?>
							<option  class="item" value="<?php echo $month_row->month; ?> - <?php echo $month_row->year; ?>"><?php echo $month_row->month; ?> - <?php echo $month_row->year; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="cs-section-content">
            	<div class="cs-section-main width-100">
					
					<div class="cs-successlist-tbl-header">
						<table class='wp-list-table widefat fixed striped posts'>
							<thead>
								<tr>
									<th class="manage-column ss-list-width" style="width:50px;">#</th>
									<th class="manage-column ss-list-width" style="width:30%">Name</th>
									<th class="manage-column ss-list-width">Country</th>
									<th class="manage-column ss-list-width">Visa Type</th>
									<th class="manage-column ss-list-width">Result</th>
									
									<th style="display:none;" class="manage-column ss-list-width">Month</th>
									<th class="manage-column ss-list-width">Month - Year</th>
								</tr>
							</thead>
						</table>
					</div>
					<div class="cs-successlist-tbl-content">
						<table class='wp-list-table widefat fixed striped posts' id="cs_successlist_tbl">
							<tbody>
							<?php foreach ($rows as $row) { ?>
								<tr>
									<td class="manage-column ss-list-width" style="width:50px;"><?php echo $count; ?></td>
									<td class="manage-column ss-list-width" style="width:30%"><?php echo $row->name; ?></td>
									<td class="manage-column ss-list-width"><?php echo $row->country; ?></td>
									<td class="manage-column ss-list-width"><?php echo $row->visa_type; ?></td>
									<td class="manage-column ss-list-width"><?php echo $row->results; ?></td>
									
									<td style="display:none;" class="manage-column ss-list-width"><?php echo $row->month; ?> - <?php echo $row->year; ?></td>
									<td class="manage-column ss-list-width month-year"><?php echo substr($row->month, 0, 3); ?> - <?php echo $row->year; ?></td>
								</tr>
							<?php $count++; } ?>
							</tbody>
						</table>
					</div>
				</div>

			</div>
			
		</div>
		<script>
			jQuery('#cs_successListFilter').change(function () {
				var select, filter, table, tr, td, i, txtValue;
				select = jQuery(this);
				filter = jQuery(this).val();
				table = document.getElementById("cs_successlist_tbl");
				tr = table.getElementsByTagName("tr");
				for (i = 0; i < tr.length; i++) {
					td = tr[i].getElementsByTagName("td")[5];
					if (td) {
						txtValue = td.textContent || td.innerText;
						if (txtValue.indexOf(filter) > -1) {
							tr[i].style.display = "";
						} else {
							tr[i].style.display = "none";
						}
					}
					
				}
				//alert(filter);
				if(filter == ''){
					var baseTitle = "All Success List";
					jQuery('#base-title').html(baseTitle);
				} else{
					var baseTitle = "Success List for the month of <span>" + filter + "</span>";
					jQuery('#base-title').html(baseTitle);
				}
				
			})
		</script>
	<?php
	echo ob_get_clean();
 }

 add_shortcode( 'success-list', 'cs_success_list_shortcode' );